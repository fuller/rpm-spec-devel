#!/bin/bash

for f in *.spec
do
    LABEL=${f%.spec}
	PACK="fuller"
	PROJ="terraform"
	URLBASE=https://$PACK.fedorapeople.org/$PROJ

	python3 packager.py -n -c $LABEL -p $PACK --spec $URLBASE/$f --srpm $URLBASE/$LABEL-*.src.rpm
done

