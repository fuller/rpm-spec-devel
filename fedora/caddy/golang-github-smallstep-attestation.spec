# Generated by go2rpm 1.9.0
%bcond_without check
%global debug_package %{nil}

# https://github.com/smallstep/go-attestation
%global goipath         github.com/smallstep/go-attestation
Version:                0.4.3
%global commit          270ecbab1f21f1a2f10d30553a5666d2ae01fbbc

%gometa -f


%global common_description %{expand:
testing for smallstep}

%global golicenses      LICENSE
%global godocs          CONTRIBUTING.md README.md

Name:           %{goname}
Release:        %autorelease
Summary:        None

License:        Apache-2.0
URL:            %{gourl}
Source:         %{gosource}

%description %{common_description}

%gopkg

%prep
%goprep
%autopatch -p1

%generate_buildrequires
%go_generate_buildrequires

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE
%doc docs CONTRIBUTING.md README.md

%gopkgfiles

%changelog
%autochangelog
