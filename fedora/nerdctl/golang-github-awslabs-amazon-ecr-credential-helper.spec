# Generated by go2rpm 1.9.0
%bcond_without check
%global debug_package %{nil}

# https://github.com/awslabs/amazon-ecr-credential-helper
%global goipath         github.com/awslabs/amazon-ecr-credential-helper
Version:                0.7.1

%gometa -f


%global common_description %{expand:
Automatically gets credentials for Amazon ECR on docker push/docker pull.}

%global golicenses      LICENSE NOTICE THIRD-PARTY-LICENSES
%global godocs          docs CODE_OF_CONDUCT.md CONTRIBUTING.md CHANGELOG.md\\\
                        README.md

Name:           %{goname}
Release:        %autorelease
Summary:        Automatically gets credentials for Amazon ECR on docker push/docker pull

License:        Apache-2.0
URL:            %{gourl}
Source:         %{gosource}

%description %{common_description}

%gopkg

%prep
%goprep
%autopatch -p1

%generate_buildrequires
%go_generate_buildrequires

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE NOTICE THIRD-PARTY-LICENSES
%doc docs CODE_OF_CONDUCT.md CONTRIBUTING.md CHANGELOG.md README.md

%gopkgfiles

%changelog
%autochangelog
