# Generated by go2rpm 1.9.0
# disable tests: missing datafiles
%bcond_with check
%global debug_package %{nil}

# https://github.com/multiformats/go-multihash
%global goipath         github.com/multiformats/go-multihash
Version:                0.2.2

%gometa -f


%global common_description %{expand:
Multihash implementation in Go.}

%global golicenses      LICENSE multihash/LICENSE
%global godocs          README.md multihash/README.md opts/README.md

Name:           %{goname}
Release:        %autorelease
Summary:        Multihash implementation in Go

License:        MIT
URL:            %{gourl}
Source:         %{gosource}

%description %{common_description}

%gopkg

%prep
%goprep
%autopatch -p1

%generate_buildrequires
%go_generate_buildrequires

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE multihash/LICENSE
%doc README.md multihash/README.md opts/README.md

%gopkgfiles

%changelog
%autochangelog
