# Generated by go2rpm 1.9.0
%bcond_without check
%global debug_package %{nil}

# https://github.com/Microsoft/hcsshim
%global goipath         github.com/Microsoft/hcsshim
Version:                0.9.8

%gometa -f


%global common_description %{expand:
Windows - Host Compute Service Shim.}

%global golicenses      LICENSE
%global godocs          README.md SECURITY.md

Name:           %{goname}
Release:        %autorelease
Summary:        Windows - Host Compute Service Shim

License:        Apache-2.0 AND MIT
URL:            %{gourl}
Source:         %{gosource}

%description %{common_description}

%gopkg

%prep
%goprep
%autopatch -p1

%generate_buildrequires
%go_generate_buildrequires

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE
%doc README.md SECURITY.md

%gopkgfiles

%changelog
%autochangelog
