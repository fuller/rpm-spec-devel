# Generated by go2rpm 1.9.0
# disable tests for now
%bcond_with check
%global debug_package %{nil}

# https://github.com/segmentio/asm
%global goipath         github.com/segmentio/asm
Version:                1.2.0

%gometa -f


%global common_description %{expand:
Go library providing algorithms optimized to leverage the characteristics of
modern CPUs.}

%global golicenses      LICENSE
%global godocs          CODE_OF_CONDUCT.md CONTRIBUTING.md README.md

Name:           %{goname}
Release:        %autorelease
Summary:        Go library providing algorithms optimized to leverage the characteristics of modern CPUs

License:        MIT
URL:            %{gourl}
Source:         %{gosource}

%description %{common_description}

%gopkg

%prep
%goprep
%autopatch -p1

%generate_buildrequires
%go_generate_buildrequires

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE
%doc CODE_OF_CONDUCT.md CONTRIBUTING.md README.md

%gopkgfiles

%changelog
%autochangelog
