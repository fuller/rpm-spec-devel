#
# spec file for package $YOUR_PACKAGE
#
# Copyright (c) $CURRENT_YEAR $YOUR_NAME_WITH_MAIL_ADDRESS
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

%global provider_prefix github.com/example/repo
%global import_path     %{provider_prefix} 

Name:           golang-$STRING
Version:        0.0.0+git20140916
Release:        0
License:        $LICENSE
Summary:        $SUMMARY
Url:            $HOMEPAGE
Group:          Development/Languages/Go
Source:         $EXACT_UPSTREAM_NAME-%{version}.tar.xz
BuildRequires:  golang-packaging
BuildRequires:  xxx-devel
BuildRequires:  xz
Requires:       xxx-devel

%{go_nostrip}
%{go_provides}

%description
$DESCRIPTION
 
%prep
%setup -q -n $EXACT_UPSTREAM_NAME-%{version}

%build
%goprep %{import_path}
%gobuild .

%install
%goinstall
%gosrc

%gofilelist

%check
%gotest %{import_path}

%files -f file.lst
%license LICENSE
%doc README

%changelog
