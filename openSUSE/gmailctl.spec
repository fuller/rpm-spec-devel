#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
 
# Please submit bugfixes or comments via https://bugs.opensuse.org/
# 

%global provider_prefix github.com/mbrt/gmailctl
%global import_path     %{provider_prefix}

Name:           gmailctl
Version:        0.10.4
Release:        0
License:        BSD and MIT
Summary:        Declarative configuration for Gmail filters
Url:            https://github.com/mbrt/gmailctl
Group:          Development/Languages/Go
Source0:        https://%{provider_prefix}/archive/refs/tags/v%{version}.tar.gz
Source1:        vendor.tar.gz

%description
Declarative configuration for Gmail filters
 
%prep
%setup -q -n gmailctl-%{version}

%build
%goprep %{import_path}

for cmd in cmd/* ; do
  %gobuild %{gobuilddir}/bin/$(basename $cmd) %{provider_prefix}/$cmd
done


%install
%goinstall
%gosrc

%gofilelist

%check
%gotest %{import_path}

%files -f file.lst
%license LICENSE
%doc docs README.md

%changelog
* Sun Jul 17 2022 Mark E. Fuller <fuller@fedoraproject.org> - 3.14.0-0
- Initial package
