#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
 
# Please submit bugfixes or comments via https://bugs.opensuse.org/
# 
%global provider_prefix github.com/go-task/task
%global import_path     %{provider_prefix}

Name:           task
Version:        3.14.1
Release:        0
License:        MIT
Summary:        A task runner / simpler Make alternative written in Go
Url:            https://taskfile.dev
Group:          Development/Languages/Go
Source0:        %{name}-%{version}.tar.gz
Source1:        vendor.tar.gz
BuildRequires:  golang-packaging

%{go_nostrip}
%{go_provides}

%description
A task runner / simpler Make alternative written in Go
 
%prep
%setup -q -n %{name}-%{version}

%build
%goprep %{import_path}
go build -mod=vendor -o %{gobuilddir}/bin/%{name} %{goipath}/%{name}

%install
install -m 0755 -vd                            %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/%{name}  %{buildroot}%{_bindir}/%{name}

%check
%gotest %{import_path}

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_bindir}/*
%doc docs

%changelog
* Sat Aug 06 2022 Mark E. Fuller <fuller@fedoraproject.org> - 3.14.1-1
- Initial package
