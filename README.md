# rpm-spec-devel

work-in-progress spec files for packaging

Fedora:
- all dependent go packages each get their own RPM

openSUSE:
- bundle dependent source code:
	go mod vendor
	tar -czf vendor.tar.gz vendor
- set second source tarball as vendor.tar.gz
